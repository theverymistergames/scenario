﻿# MisterGames Scenario v0.2.0

## Usage
- todo

## Assembly definitions
- MisterGames.Scenario

## Dependencies
- MisterGames.Common

## Installation
- Add [MisterGames Common](https://gitlab.com/theverymistergames/common/) package
- Top menu MisterGames -> Packages, add packages: 
  - [Scenario](https://gitlab.com/theverymistergames/scenario/)